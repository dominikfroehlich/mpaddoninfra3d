import Routing from "./components/RoutingTool.vue";
import RoutingStore from "./store/index";
import deLocale from "./locales/de/additional.json";
import enLocale from "./locales/en/additional.json";

export default {
    component: Routing,
    store: RoutingStore,
    locales: {
        de: deLocale,
        en: enLocale
    }
};
