
/**
 * User type definition
 * @typedef {object} RoutingState
 */
const state = {
    from_address: {
        coordinates: []
    },
    to_address: {
        coordinates: []
    }
};

export default state;
