/**
 * User type definition
 * @typedef {object} RoutingState
 */
const state = {
    activeTab: 1,
    creatingVehicle: false,
    vehicles: [],
    creatingJob: false,
    jobs: []
};

export default state;
