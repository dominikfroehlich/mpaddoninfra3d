/**
 * @event Animation#changeIsActive
 * @memberof Addons
 * @description Is fired when the models attribute "isActive" changes
*/

/**
 * @event Animation#render
 * @memberof Addons
*/
