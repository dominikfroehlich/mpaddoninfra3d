/**
 * @event PerformanceTester#changeIsActive
 * @memberof Addons
 * @description Is fired when the models attribute "isActive" changes
*/
